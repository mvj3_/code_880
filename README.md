自从iOS3.2以来，Apple允许我们在自己的app中使用自定义的字体。但是这一过程并不简单，因为我们不能在

    Interface Builder （NIB）文件中之间给控件使用我们自己的字体。如果你使用NIB来制作UI，有很多控件需要定义各种自己的字体，这一过程十分麻烦，你必须要在程序里逐个写代码使用自定义字体。FontReplacer很好地解决了这个问题，至少比之前简便很多。使用FontReplacer方法如下：比如你在NIB中设置某个UILable的字体是Arial，但是你想这个UILable使用的是你自己的Caviar Dreams字体，那么你只要在代码中定义好你的字体和Arial的映射，剩下的runtime转换就交给FontReplacer了：

    ReplacementFonts = {

     “ArialMT” = “CaviarDreams”;

     “Arial-ItalicMT” = “CaviarDreams-Italic”;

     “Arial-BoldMT” = “CaviarDreams-Bold”;

     “Arial-BoldItalicMT” = “CaviarDreams-BoldItalic”;

    };

    下面屏幕截图中，左图是在nib中的截图，右图是程序运行中的截图。可以看到，FontReplacer在程序运行时将lable的字体替换成自己定义的字体了。